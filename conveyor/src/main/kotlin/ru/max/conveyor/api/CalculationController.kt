package ru.max.conveyor.api

import lombok.extern.slf4j.Slf4j
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RestController
import ru.max.conveyor.api.controller.CalculationApi
import ru.max.conveyor.api.dto.CreditDTO
import ru.max.conveyor.api.dto.ScoringDataDTO
import ru.max.conveyor.service.CalculationService

@Slf4j
@RestController
class CalculationController(
    private val calculationService: CalculationService
) : CalculationApi {

    override fun calculation(scoringDataDTO: ScoringDataDTO): ResponseEntity<CreditDTO> {
        return ResponseEntity.ok(calculationService.calculateDate(scoringDataDTO))
    }
}