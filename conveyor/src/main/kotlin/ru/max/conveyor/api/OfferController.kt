package ru.max.conveyor.api

import lombok.extern.slf4j.Slf4j
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RestController
import ru.max.conveyor.api.controller.OfferApi
import ru.max.conveyor.api.dto.LoanApplicationRequestDTO
import ru.max.conveyor.api.dto.LoanOfferDTO
import ru.max.conveyor.service.OfferService

@Slf4j
@RestController
class OfferController(
    private val offerService: OfferService
) : OfferApi {
    override fun offer(loanApplicationRequestDTO: LoanApplicationRequestDTO): ResponseEntity<List<LoanOfferDTO>> {
        return ResponseEntity.ok(offerService.offer(loanApplicationRequestDTO))
    }
}