package ru.max.conveyor.service

import lombok.extern.slf4j.Slf4j
import org.springframework.stereotype.Service
import ru.max.conveyor.api.dto.CreditDTO
import ru.max.conveyor.api.dto.PaymentScheduleElement
import ru.max.conveyor.api.dto.ScoringDataDTO
import ru.max.conveyor.exception.ValidationException
import java.math.BigDecimal
import java.math.RoundingMode
import java.time.LocalDate
import java.time.Period
import java.time.temporal.ChronoUnit

private val basePeriodInterestRate = BigDecimal(15)
private val monthsInYear = BigDecimal(12)

@Slf4j
@Service
class CalculationServiceImpl : CalculationService {

    override fun calculateDate(scoringDataDTO: ScoringDataDTO): CreditDTO {
        return if (scoringDataDTO.paymentType == ScoringDataDTO.PaymentType.annuity) getAnnuityPaymentCreditDTO(
            scoringDataDTO
        )
        else getDifferentiatedPaymentsCreditDTO(scoringDataDTO)
    }
}

fun getAnnuityPaymentCreditDTO(scoringDataDTO: ScoringDataDTO): CreditDTO {
    lateinit var creditDTO: CreditDTO
    lateinit var createPeriodList: List<PaymentScheduleElement>
    lateinit var rate: BigDecimal
    val amount = scoringDataDTO.amount
    val term = scoringDataDTO.term
    val monthlyAnnuityPayment: BigDecimal
    val psk: BigDecimal
    try {
        rate = getResultInterestRate(scoringDataDTO, basePeriodInterestRate)
        monthlyAnnuityPayment = getMonthlyAnnuityPayment(term, amount, rate)
        psk = monthlyAnnuityPayment.times(BigDecimal(term))
        val totalPayment = getPartOfPrincipalDebt(psk, term)

        createPeriodList = createAnnuityPaymentByPeriod(amount, totalPayment, term, LocalDate.now(), rate)
    } catch (e: Exception) {
        throw RuntimeException("Refusal")
    }

    creditDTO = CreditDTO(
        amount = amount,
        term = term,
        monthlyPayment = monthlyAnnuityPayment,
        rate = rate,
        psk = psk,
        isInsuranceEnabled = true,
        isSalaryClient = true,
        paymentSchedule = createPeriodList
    )
    return creditDTO
}

fun getDifferentiatedPaymentsCreditDTO(scoringDataDTO: ScoringDataDTO): CreditDTO {
    lateinit var creditDTO: CreditDTO
    lateinit var createPeriodList: List<PaymentScheduleElement>
    lateinit var rate: BigDecimal
    val averageMonthlyDifferentiatedPayment: BigDecimal
    val pskDifferentiatedPayment: BigDecimal
    val amount = scoringDataDTO.amount
    val term = scoringDataDTO.term
    try {
        rate = getResultInterestRate(scoringDataDTO, basePeriodInterestRate)
        pskDifferentiatedPayment = getPSK(amount, term, rate)
        createPeriodList =
            createDifferentiatedPaymentsByPeriod(amount, term, LocalDate.now(), rate)
        averageMonthlyDifferentiatedPayment =
            createPeriodList
                .map { it.totalPayment.toDouble() }
                .filter { !it.equals(0.0) }
                .average()
                .toBigDecimal()
                .setScale(2, RoundingMode.HALF_UP)

    } catch (e: Exception) {
        throw RuntimeException("Refusal")
    }
    creditDTO = CreditDTO(
        amount = amount,
        term = term,
        monthlyPayment = averageMonthlyDifferentiatedPayment,
        rate = rate,
        psk = pskDifferentiatedPayment,
        isInsuranceEnabled = true,
        isSalaryClient = true,
        paymentSchedule = createPeriodList
    )
    return creditDTO
}

fun getPSK(
    amount: BigDecimal,
    term: Int,
    interestRate: BigDecimal
): BigDecimal {
    val percentOnMonth = interestRate.divide(monthsInYear, 2, RoundingMode.HALF_UP)
    val sumPercent = percentOnMonth.multiply(BigDecimal(term)).movePointLeft(2)
    val overpaymentAmount = amount.multiply(sumPercent).setScale(2, RoundingMode.HALF_UP)
    return amount.plus(overpaymentAmount)
}

fun getMonthlyAnnuityPayment(
    term: Int,
    amount: BigDecimal,
    annualInterestRate: BigDecimal
): BigDecimal {
    val monthlyInterestRate = annualInterestRate.divide(monthsInYear, 4, RoundingMode.HALF_UP).movePointLeft(2)
    val sum = monthlyInterestRate.plus(BigDecimal.ONE)
    val pow = sum.pow(term)
    val minus = pow.minus(BigDecimal.ONE)
    val times = monthlyInterestRate.times(pow)
    val annuityRatio = times.div(minus)
    return amount.times(annuityRatio).setScale(2, RoundingMode.HALF_UP)
}

fun getPartOfPrincipalDebt(
    amount: BigDecimal,
    term: Int
): BigDecimal = amount.divide(BigDecimal(term), 2, RoundingMode.HALF_UP)

fun createAnnuityPaymentByPeriod(
    amount: BigDecimal,
    totalPayment: BigDecimal,
    term: Int,
    startDate: LocalDate,
    rate: BigDecimal
): List<PaymentScheduleElement> {
    var remainingDebt = amount
    var count = 2

    val pairList = generateSequence(startDate.plusMonths(1)) { it.plusMonths(1) }
        .map {
            val interestPayment = getInterestPayment(
                remainingDebt,
                rate,
                getCountDaysInMonth(it),
                it.lengthOfYear()
            )
            val debtPayment = totalPayment.minus(interestPayment)
            remainingDebt = remainingDebt.minus(debtPayment)
            PaymentScheduleElement(
                count++,
                it,
                totalPayment,
                interestPayment,
                debtPayment,
                remainingDebt
            )
        }
        .takeWhile { it.date != startDate.plusMonths(term.toLong()) }
        .toMutableList()
    pairList.add(0, PaymentScheduleElement(1, startDate, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, amount))
    return pairList
}

fun createDifferentiatedPaymentsByPeriod(
    amount: BigDecimal,
    term: Int,
    startDate: LocalDate,
    resultInterestRate: BigDecimal
): List<PaymentScheduleElement> {
    var remainingDebt = amount
    var count = 2
    val partOfPrincipalDebt = getPartOfPrincipalDebt(amount, term)

    val pairList = generateSequence(startDate.plusMonths(1)) { it.plusMonths(1) }
        .map {
            remainingDebt = remainingDebt.minus(partOfPrincipalDebt)
            val interestPayment = getInterestPayment(
                remainingDebt,
                resultInterestRate,
                getCountDaysInMonth(it),
                it.lengthOfYear()
            )
            val totalPayment = interestPayment.plus(partOfPrincipalDebt)
            PaymentScheduleElement(
                count++,
                it,
                totalPayment,
                interestPayment,
                partOfPrincipalDebt,
                remainingDebt
            )
        }
        .takeWhile { it.date != startDate.plusMonths(term.toLong()) }
        .toMutableList()
    pairList.add(0, PaymentScheduleElement(1, startDate, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, amount))
    return pairList
}

fun getInterestPayment(
    outstandingBalance: BigDecimal,
    resultInterestRate: BigDecimal,
    daysOnMonth: Int,
    daysOnYear: Int
): BigDecimal {
    val outstandingInterestRate = outstandingBalance.multiply(resultInterestRate).movePointLeft(2)
    val multiply = outstandingInterestRate.multiply(BigDecimal(daysOnMonth))
    return multiply.divide(BigDecimal(daysOnYear), 2, RoundingMode.HALF_UP)
}

fun getCountDaysInMonth(date: LocalDate): Int {
    val previousMonth = date.minusMonths(1)
    return previousMonth.until(date, ChronoUnit.DAYS).toInt()
}

private fun getResultInterestRate(
    scoringDataDTO: ScoringDataDTO,
    basedInterestRate: BigDecimal
): BigDecimal {
    return ArrayList<BigDecimal>(
        arrayListOf(
            getScoringByEmploymentStatus(scoringDataDTO),
            getScoringByPosition(scoringDataDTO),
            getScoringByAmount(scoringDataDTO),
            getScoringByMaritalStatus(scoringDataDTO),
            getScoringByDependentAmount(scoringDataDTO),
            getScoringByAge(scoringDataDTO),
            getScoringByGender(scoringDataDTO),
            getScoringByWorkExperience(scoringDataDTO)
        )
    ).fold(BigDecimal.ZERO, BigDecimal::plus)
        .plus(basedInterestRate)
}

private fun getScoringByEmploymentStatus(scoringDataDTO: ScoringDataDTO): BigDecimal {
    return when (scoringDataDTO.employment.employmentStatus.name) {
        "unemployed" -> throw ValidationException("Refusal")
        "selfEmployed" -> BigDecimal.ONE
        "businessOwner" -> BigDecimal(3)
        else -> BigDecimal.ZERO
    }
}

private fun getScoringByPosition(scoringDataDTO: ScoringDataDTO): BigDecimal {
    return when (scoringDataDTO.employment.position.name) {
        "middleManager" -> BigDecimal(-2)
        "topManager" -> BigDecimal(-4)
        else -> BigDecimal.ZERO
    }
}

private fun getScoringByAmount(scoringDataDTO: ScoringDataDTO): BigDecimal {
    return if (scoringDataDTO.amount > scoringDataDTO.employment.salary.multiply(BigDecimal(20))
    ) {
        throw ValidationException("Refusal")
    } else BigDecimal.ZERO
}

private fun getScoringByMaritalStatus(scoringDataDTO: ScoringDataDTO): BigDecimal {
    return when (scoringDataDTO.maritalStatus.name) {
        "married" -> BigDecimal(-3)
        "single" -> BigDecimal(1)
        else -> BigDecimal.ZERO
    }
}

private fun getScoringByDependentAmount(scoringDataDTO: ScoringDataDTO): BigDecimal {
    return if (scoringDataDTO.dependentAmount > 1) BigDecimal.ONE else BigDecimal.ZERO
}

private fun getScoringByAge(scoringDataDTO: ScoringDataDTO): BigDecimal {
    val age = getAge(scoringDataDTO)

    return if (age in 20..60) BigDecimal.ZERO else throw ValidationException("Refusal")
}

private fun getScoringByGender(scoringDataDTO: ScoringDataDTO): BigDecimal {
    val age = getAge(scoringDataDTO)
    val female = age in 35..60
    val male = age in 30..55

    return when (scoringDataDTO.gender.name) {
        "male" -> if (male) BigDecimal(-3) else BigDecimal.ZERO
        "female" -> if (female) BigDecimal(-3) else BigDecimal.ZERO
        "notBinary" -> BigDecimal(3)
        else -> BigDecimal.ZERO
    }
}

private fun getScoringByWorkExperience(scoringDataDTO: ScoringDataDTO): BigDecimal {
    return if (scoringDataDTO.employment.workExperienceTotal < 12
        || scoringDataDTO.employment.workExperienceCurrent < 3
    ) throw ValidationException("Refusal")
    else BigDecimal.ZERO
}

private fun getAge(scoringDataDTO: ScoringDataDTO): Int {
    return Period.between(scoringDataDTO.birthdate, LocalDate.now()).years
}