package ru.max.conveyor.service

import ru.max.conveyor.api.dto.LoanApplicationRequestDTO
import ru.max.conveyor.api.dto.LoanOfferDTO

interface OfferService {
    fun offer(loanApplicationRequestDTO: LoanApplicationRequestDTO): List<LoanOfferDTO>
}