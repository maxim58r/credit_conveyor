package ru.max.conveyor.service

import ru.max.conveyor.api.dto.CreditDTO
import ru.max.conveyor.api.dto.ScoringDataDTO

interface CalculationService {
    fun calculateDate(scoringDataDTO: ScoringDataDTO): CreditDTO
}