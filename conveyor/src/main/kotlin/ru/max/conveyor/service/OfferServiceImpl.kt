package ru.max.conveyor.service

import lombok.extern.slf4j.Slf4j
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import ru.max.conveyor.api.dto.LoanApplicationRequestDTO
import ru.max.conveyor.api.dto.LoanOfferDTO
import java.math.BigDecimal

@Slf4j
@Service
class OfferServiceImpl : OfferService {
    @Value("\${offer.rate}")
    private lateinit var rate: BigDecimal


    override fun offer(loanApplicationRequestDTO: LoanApplicationRequestDTO): List<LoanOfferDTO> {

        println(rate)
        TODO("Not yet implemented")
    }

    private fun getCreditOffers(
        isInsuranceEnabled: Boolean,
        isSalaryClient: Boolean
    ): BigDecimal {
        return when {
            isInsuranceEnabled && isSalaryClient -> BigDecimal(-4)
            isInsuranceEnabled && !isSalaryClient -> BigDecimal(-2)
            !isInsuranceEnabled && !isSalaryClient -> BigDecimal(4)
            else -> BigDecimal(2)
        }
    }

    private fun getLoanOfferDTO(loanApplicationRequestDTO: LoanApplicationRequestDTO) {
        LoanOfferDTO(
            applicationId = 1,
            requestedAmount = loanApplicationRequestDTO.amount,
            totalAmount = BigDecimal.ZERO,
            term = loanApplicationRequestDTO.term,
            monthlyPayment = BigDecimal.ZERO,
            rate = BigDecimal.ZERO,
            isInsuranceEnabled = true,
            isSalaryClient = true
        )
    }
    fun solution(names: List<String>): Int {
        return names.filterIndexed { inx, it -> inx % 2 == 0 && it.startsWith("T") }.first().indices.first
    }
}