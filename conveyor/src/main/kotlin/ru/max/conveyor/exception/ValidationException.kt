package ru.max.conveyor.exception

class ValidationException: RuntimeException {
    constructor(message: String)
}