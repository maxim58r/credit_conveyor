-- liquibase formatted sql

--changeset mserov:1

create table users
(
    id          bigserial
        primary key,
    username    varchar(64) not null
        unique,
    birth_date  date,
    firstname   varchar(64),
    lastname    varchar(64),
    role        varchar(32),
    created_at  timestamp,
    modified_at timestamp,
    created_by  varchar(32),
    modified_by varchar(32)
);
--rollback DROP TABLE users;