package ru.max.conveyor.model

import ru.max.conveyor.api.dto.CreditDTO
import ru.max.conveyor.api.dto.PaymentScheduleElement
import java.math.BigDecimal
import java.time.LocalDate

class ModelObjectForResponse {

    private fun getPaymentScheduleElement() = PaymentScheduleElement(
        number = 1,
        date = LocalDate.now(),
        totalPayment = BigDecimal(150000.25),
        interestPayment = BigDecimal(3800.54),
        debtPayment = BigDecimal(1700.85),
        remainingDebt = BigDecimal(50000.54)
    )

    fun getCreditDTO() = CreditDTO(
        amount = BigDecimal(150000.55),
        term = 36,
        monthlyPayment = BigDecimal(6000.55),
        rate = BigDecimal(9.85),
        psk = BigDecimal(200560.21),
        isInsuranceEnabled = true,
        isSalaryClient = true,
        paymentSchedule = listOf(getPaymentScheduleElement())

    )
}