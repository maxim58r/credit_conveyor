package ru.max.conveyor.model

import ru.max.conveyor.api.dto.EmploymentDTO
import ru.max.conveyor.api.dto.ScoringDataDTO
import java.math.BigDecimal
import java.time.LocalDate

class ModelObjectForRequest {
    fun getScoringDataDTO() =
        ScoringDataDTO(
            amount = BigDecimal(111000.22),
            term = 48,
            firstName = "Максим",
            lastName = "Серов",
            middleName = "Валерьевич",
            gender = ScoringDataDTO.Gender.male,
            birthdate = LocalDate.of(1980, 11, 24),
            passportSeries = "4505",
            passportNumber = "458357",
            passportIssueDate = LocalDate.of(2002, 3, 24),
            passportIssueBranch = "УМВД Ленинского района г. Москва",
            maritalStatus = ScoringDataDTO.MaritalStatus.married,
            dependentAmount = 2,
            employment = getEmployment(),
            account = "30301810900006004000",
            isInsuranceEnabled = true,
            isSalaryClient = true
        )

    fun getEmployment() =
        EmploymentDTO(
            employmentStatus = EmploymentDTO.EmploymentStatus.businessOwner,
            employerINN = "7707083893",
            salary = BigDecimal(98000),
            position = EmploymentDTO.Position.middleManager,
            workExperienceTotal = 36,
            workExperienceCurrent = 15
        )

}