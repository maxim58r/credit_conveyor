package ru.max.conveyor.service

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import ru.max.conveyor.model.ModelObjectForRequest
import ru.max.conveyor.model.ModelObjectForResponse

class CalculationServiceImplTest {

    private val request = ModelObjectForRequest()
    private val response = ModelObjectForResponse()
    private val validationService = CalculationServiceImpl()

    @Test
    fun testValidateDateOk() {
//        val test: ValidationService = mock(ValidationService::class.java)
//        `when`(test.validateDate(request.getScoringDataDTO())).thenReturn(response.getCreditDTO())
        assertEquals(response.getCreditDTO(), validationService.calculateDate(request.getScoringDataDTO()))
    }

}